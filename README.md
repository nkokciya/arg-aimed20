# ARG-AIMed

This Python implementation shows the applicability of our approach to two medical scenarios, Joey and Rachel, as described in the paper. 

Our paper called "Argumentation Schemes for Clinical Decision Support" was submitted to Argument & Computation journal; and it is currently under review. 

*  [DLV](http://www.dlvsystem.com/dlv/) should be installed on the system in order to run the code. 
*  In the root directory, the following code should be invoked on the terminal:
`python evalaf.py kb.dl attacks.dl joey/joey.dl` or `python evalaf.py kb.dl attacks.dl rachel/rachel-tX.dl`
* After running the code, the terminal will display acceptable arguments. In each case-study folder, the corresponding graphs will be generated. 
* 'outputs' folder already contains the outputs that you should get after running code.
